class CreateReservations < ActiveRecord::Migration[5.0]
  def change
    create_table :reservations do |t|
      t.integer :course
      t.references :user
      t.timestamp :term

      t.timestamps
    end

    add_foreign_key :reservations, :users
  end
end
