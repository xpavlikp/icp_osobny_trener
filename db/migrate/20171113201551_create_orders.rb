class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.references :user
      t.references :product
      t.integer :quantity

      t.timestamps
    end

    add_foreign_key :orders, :users
    add_foreign_key :orders, :products
  end
end
