class StaticPagesController < ApplicationController
  def home
  end

  def achievements
  end

  def contact
  end

  def discussions
  end

  def archive
  end

  def faq
  end
end
