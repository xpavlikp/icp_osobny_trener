class User < ApplicationRecord
	has_many :order, :dependent => :destroy
	has_many :reservation, :dependent => :destroy
	before_save { self.email = email.downcase }
	validates :name,  presence: true, length: { maximum: 50 }
  	validates :email, presence: true, length: { maximum: 255 },
                    uniqueness: { case_sensitive: false }
    has_secure_password
    validates :password, presence: true, length: { minimum: 6 }
end
