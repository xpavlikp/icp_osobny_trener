json.extract! reservation, :id, :type, :user_id, :term, :created_at, :updated_at
json.url reservation_url(reservation, format: :json)
