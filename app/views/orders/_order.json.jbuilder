json.extract! order, :id, :user, :product, :quantity, :created_at, :updated_at
json.url order_url(order, format: :json)
