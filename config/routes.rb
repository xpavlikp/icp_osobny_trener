Rails.application.routes.draw do

  resources :orders
  resources :reservations
  resources :products
  resources :articles
  get 'sessions/new'

  get 'users/new'

  root 'static_pages#home'
  get  '/achievements', to: 'static_pages#achievements'
  get  '/contact', to: 'static_pages#contact'
  get  '/discussions', to: 'static_pages#discussions'
  get  '/faq', to: 'static_pages#faq'
  get  '/archive', to: 'static_pages#archive'
  get  '/signup',  to: 'users#new'
  post '/signup',  to: 'users#create'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  resources :users

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
